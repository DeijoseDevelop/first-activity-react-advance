import {Route, Routes} from 'react-router-dom';
import Login from './pages/Login';
import Contacto from './pages/Contacto';
import Portada from './pages/Portada';
import Header from './components/Header';
import Informacion from './pages/Informacion';
import ThemeProvider from './context/theme.context';
import { LoginProvider, sessionInfo } from './context/LoginContext';

function App() {
  return (
    <ThemeProvider>
      <LoginProvider initialValueSession={sessionInfo}>
        <div>
          <Header />
          <Routes>
            <Route path='/' element={<Login />} />
            <Route path='/portada' element={<Portada />}></Route>
            <Route path='/contacto' element={<Contacto />} />
            <Route path='/informacion' element={<Informacion />} />
          </Routes>
        </div>
        </LoginProvider>
    </ThemeProvider>
  );
}

export default App;
