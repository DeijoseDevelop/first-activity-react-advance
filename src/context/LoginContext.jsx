import {useState, createContext} from 'react';

const sessionInfo = {
  isLogged: false,
  token: ''
};

const LoginContext = createContext(sessionInfo);

const {Provider, consumer} = LoginContext;

const LoginProvider = ({initialValueSession, children}) => {
    const [session, setSession] = useState(initialValueSession)
  const changeStateSession = (newSession) => {
    setSession(newSession);
  };
  return <Provider value={{session, changeStateSession}}>{children}</Provider>;
};
export {LoginProvider, consumer as LoginConsumer, LoginContext, sessionInfo};
