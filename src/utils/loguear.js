import instance from './instanceApi';

async function loguear({email, password}) {
  let res = await instance.post('/login', {
    email: email,
    password: password,
  });
  return res.data;
}
export default loguear;
