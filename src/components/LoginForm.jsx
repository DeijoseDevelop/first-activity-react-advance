import {Formik, Form, Field} from 'formik';

export default function LoginForm({onSubmit, referencia}) {
  const initialData = {
    email: '',
    password: '',
  };
  const submit = (values, actions) => {
    actions.setSubmitting(true);
    onSubmit(values);
    actions.resetForm();
    actions.setSubmitting(false);
  };
  return (
    <Formik initialValues={initialData} onSubmit={submit} innerRef={referencia}>
      {() => {
        return (
          <div>
            <h1 className='text-center'>Login de Usuario</h1>
            <Form>
              <div className='form-group my-3'>
                <label htmlFor='email'>Email</label>
                <Field
                  className='form-control'
                  id='email'
                  type='email'
                  name='email'
                  placeholder='Ingrese su Correo Electrónico'
                ></Field>
              </div>
              <div className='form-group my-3'>
                <label htmlFor='pass'>Contraseña</label>
                <Field
                  className='form-control'
                  id='pass'
                  type='password'
                  name='password'
                  placeholder='Ingrese su contraseña'
                ></Field>
              </div>
            </Form>
          </div>
        );
      }}
    </Formik>
  );
}
