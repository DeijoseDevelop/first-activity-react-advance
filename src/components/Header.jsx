import {Fragment, useContext} from 'react';
import {Link} from 'react-router-dom';
import {ThemeContext} from '../context/theme.context';

export default function Header() {
  const {toggle_theme} = useContext(ThemeContext);
  return (
    <Fragment>
      <nav className={'nav navbar-dark py-2 mb-5'}>
        <div className='mx-3 div'>
          <Link className='navbar-bran' to='/'>
            Login
          </Link>
          <Link className='navbar-bran' to='/portada'>
            Portada
          </Link>
          <Link className='navbar-bran' to='/contacto'>
            Contacto
          </Link>
          <Link className='navbar-bran' to='/informacion'>
            Información
          </Link>
        </div>
        <button onClick={() => toggle_theme()} className='btn btn-primary'>
          Tema
        </button>
      </nav>
    </Fragment>
  );
}
