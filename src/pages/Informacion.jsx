import {useState, useEffect} from 'react';
import axios from 'axios';
import Pagination from 'react-js-pagination';

export default function Informacion() {
  const [data, setData] = useState([]);
  const [page, setPage] = useState(1)

  const URL = `https://reqres.in/api/users?page=${page}`;

  useEffect(() => {
    axios({
      method: 'GET',
      url: URL,
    }).then((response) => setData(response.data.data));
  }, [page]);

  function handlePageChange(pageNumber) {
    setPage(pageNumber)
  }

  return (
    <div className='container'>
      <h1 className='text-center'>Información</h1>
      <div className='d-flex justify-content-center m-5'>
        <Pagination
          itemClass='page-item'
          linkClass='page-link'
          activePage={page}
          itemsCountPerPage={6}
          totalItemsCount={12}
          onChange={handlePageChange}
        />
      </div>
      <div className='d-flex justify-content-around flex-wrap'>
        {data ? (
          data.map((el) => {
            let {first_name, last_name, email, avatar, id} = el;
            return (
              <div
                className='card m-2 d-grid'
                key={id}
                style={{
                  placeItems: 'center',
                  width: '20rem',
                  borderRadius: '15px',
                }}
              >
                <img
                  className='card-img-top'
                  src={avatar}
                  alt='img'
                  style={{
                    borderRadius: '15px 15px 0 0',
                  }}
                />
                <div className='card-body'>
                  <h2 className='card-title text-dark'>
                    {first_name} {last_name}
                  </h2>
                  <p className='card-text text-dark'>{email}</p>
                </div>
              </div>
            );
          })
        ) : (
          <h2>Loading...</h2>
        )}
        <div className='d-flex justify-content-center m-5'>
          <Pagination
            itemClass='page-item'
            linkClass='page-link'
            activePage={page}
            itemsCountPerPage={6}
            totalItemsCount={12}
            onChange={handlePageChange}
          />
        </div>
      </div>
    </div>
  );
}
