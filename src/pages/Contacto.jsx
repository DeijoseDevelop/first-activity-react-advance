import {useRef} from 'react';
import ContactoLogin from '../components/contactoLogin';

export default function Contacto() {
  const ref = useRef(null);

  const printLogin = (values) => {
    console.log(values);
  };
  return (
    <div className='container'>
      <div className='d-grid form-fondo' style={{placeItems: 'center'}}>
        <ContactoLogin onSubmit={printLogin} innerRef={ref} />
        <button
          className='btn btn-primary'
          onClick={() => ref.current.submitForm()}
        >
          Enviar
        </button>
      </div>
    </div>
  );
}
