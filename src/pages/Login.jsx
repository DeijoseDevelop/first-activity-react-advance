import {useRef, useContext} from 'react';
import LoginForm from '../components/LoginForm';
import useLoginSession from '../hooks/useLoginSession';
import {LoginContext} from '../context/LoginContext';

export default function Login() {

  const {session} = useContext(LoginContext);

  session.isLogged
    ? localStorage.setItem('token', session.token)
    : localStorage.removeItem('token');

  const {login} = useLoginSession();
  const ref = useRef(null);
  
  const printLogin = (values) => {
    login(values.email, values.password)
    console.log(values);
  };
  return (
    <div className='container'>
      <div className='d-flex flex-column justify-content-center align-items-center form-fondo'>
        <div className='col-6'>
          <LoginForm onSubmit={printLogin} referencia={ref} />
          <button
            className='btn btn-primary'
            onClick={() => {
              if (ref.current) ref.current.submitForm();
            }}
          >
            Login
          </button>
        </div>
      </div>
    </div>
  );
}
