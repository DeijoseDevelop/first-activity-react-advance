import {useContext} from 'react';
import {LoginContext} from '../context/LoginContext';
import requestLogin from '../utils/loguear';

export default function useLoginSession() {
  const loginContext = useContext(LoginContext);

  const login = async (emailTo, passwordTo) => {
    try {
      const res = await requestLogin({
        email: emailTo,
        password: passwordTo,
      });
      loginContext.changeStateSession({
        isLogged: true,
        token: res.token,
      });
    } catch (error) {
      alert(error);
    }
  };
  return {login};
}
